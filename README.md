# Especialización en desarrollo FullStack
Desarrollo FullStack por la universidad Austral, cursado en Coursera.

## Diseñando páginas web con Bootstrap 4 - Módulo 1

### Guia Hoteles
Una web con diseño responsive del conjunto de los ejercicios realizados junto con el profesor.

### Tecnologias
- HTML / CSS / JS
- Bootstrap
- NodeJS
- Visual Studio Code